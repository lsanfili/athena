# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

import sys, tarfile, glob, random, subprocess

from PyJobTransforms.CommonRunArgsToFlags import commonRunArgsToFlags
from PyJobTransforms.TransformUtils import processPreExec, processPreInclude, processPostExec, processPostInclude
from TRT_CalibAlgs.TRTCalibrationMgrConfig import CalibConfig, TRT_CalibrationMgrCfg, TRT_StrawStatusCfg
from AthenaConfiguration.MainServicesConfig import MainServicesCfg

def nextstep(text):
    print("\n"+"#"*100)
    print("#")
    print("#    %s" % (text))
    print("#")
    print("#"*100,"\n")

def tryError(command, error):
    try:
        print(" Running: %s\n" % (command))
        stdout, stderr = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
        print("OUTPUT: \n%s" % (stdout.decode('ascii')))
        print("ERRORS: %s" % ("NONE" if stderr.decode('ascii')=='' else "\n"+stderr.decode('ascii')))
        if stderr:
            exit(1)        
    except OSError as e:
        print(error,e)
        sys.exit(e.errno)

def fromRunArgs(runArgs):
    
    inputFile  = runArgs.inputTARFile[0]
    outputFile = runArgs.outputTAR_CALIBFile
    
    ##################################################################################################
    nextstep("UNTAR input file")
    ##################################################################################################   
    
    print("Uncompressing files:")
    try:
        print("\t-",inputFile)
        tarfile.open(inputFile).extractall(".") 
    except OSError as e:
        print("ERROR: Failed uncompressing TAR file\n",e)
        sys.exit(e.errno)  
    
    ##################################################################################################
    nextstep("Renaming *straw.txt and *tracktuple.root files for the final output")
    ##################################################################################################   
    
    command  = "mv -v %s.merged.straw.txt %s.merged.straw.txt; " % (inputFile,outputFile)
    command += "mv -v %s.tracktuple.root %s.tracktuple.root; " % (inputFile,outputFile)
    
    tryError(command, "Renaming *straw.txt and *tracktuple.root files\n")
    
    ##################################################################################################
    nextstep("Calculating constants ATHENA")
    ##################################################################################################    
    
    myFile = []
    # generating the RAW file path
    if runArgs.rawfile:
        myFile.append(runArgs.rawfile)
    elif not runArgs.project or not runArgs.runnr or not runArgs.stream:
        # This part is under testing, will be further developed
        print("ERROR: project=\"%s\" or runNumber=\"%s\" or stream=\"%s\" missing..." % (runArgs.project, runArgs.runnr, runArgs.stream))
        print("Provide them!")
        sys.exit(1)
    else:
        rawpath = "/eos/atlas/atlastier0/rucio/%s/%s/%s/%s.%s.%s.merge.RAW/" % (runArgs.project,runArgs.stream,runArgs.runnr.zfill(8),runArgs.project,runArgs.runnr.zfill(8),runArgs.stream)
        globedFiles = glob.glob(rawpath+"*")
        if not globedFiles:
            print("ERROR: Not able to find any file under %s. Please check that the path is correct or files exists" % (rawpath))
            sys.exit(1)            
        
        myFile.append(random.choice(globedFiles))
        print("RAW file selected for testing:",myFile)
        if not myFile:
            print("ERROR: provide a valid project=\"%s\" or runNumber=\"%s\" or stream=\"%s\"" % (runArgs.project, runArgs.runnr, runArgs.stream))
            sys.exit(1)
    
    
    from AthenaConfiguration.AllConfigFlags import initConfigFlags    
    flags=initConfigFlags()

    commonRunArgsToFlags(runArgs, flags)

    processPreInclude(runArgs, flags)
    processPreExec(runArgs, flags)
    
    # Change this to a the hardcoded data file
    flags.Input.Files=myFile
    flags.Output.HISTFileName="DontUse.remove"
    
    # Only one event must be run 
    flags.Exec.MaxEvents=1
    
    # Importing flags - Switching off detector parts and monitoring
    CalibConfig(flags)
    
    # To respect --athenaopts 
    flags.fillFromArgs()
    
    # Reason why we need to clone and replace: https://gitlab.cern.ch/atlas/athena/-/merge_requests/68616#note_7614858
    flags = flags.cloneAndReplace(
        "Tracking.ActiveConfig",
        f"Tracking.{flags.Tracking.PrimaryPassConfig.value}Pass",
        # Keep original flags as some of the subsequent passes use
        # lambda functions relying on them
        keepOriginal=True)      

    flags.lock()
    
    cfg=MainServicesCfg(flags)
    
    from ByteStreamCnvSvc.ByteStreamConfig import ByteStreamReadCfg
    cfg.merge(ByteStreamReadCfg(flags))
    
    from InDetConfig.TrackRecoConfig import InDetTrackRecoCfg
    cfg.merge(InDetTrackRecoCfg(flags))    
    
    cfg.merge(TRT_CalibrationMgrCfg(flags,DoCalibrate=True, Hittuple=inputFile+".basic.root", caltag=runArgs.piecetoken))
    cfg.merge(TRT_StrawStatusCfg(flags))

    processPostInclude(runArgs, flags, cfg)
    processPostExec(runArgs, flags, cfg)
    
    with open("ConfigTRTCalib_calib.pkl", "wb") as f: 
        cfg.store(f)    
    
    sc = cfg.run()
    if not sc.isSuccess():
         sys.exit(not sc.isSuccess())
    
    ##################################################################################################
    nextstep("Renaming outputs from Calibrator")
    ##################################################################################################   
    
    command  =  "mv -v calibout.root %s.calibout.root ; " % (outputFile)
    command +=  "mv -v calibout_rt.txt %s.calibout_rt.txt; " % (outputFile)
    command +=  "mv -v calibout_t0.txt %s.calibout_t0.txt; " % (outputFile)
    command +=  "mv -v calib_constants_out.txt %s.calib_constants_out.txt; " % (outputFile)
    
    tryError(command, "ERROR: Failed renaming files in TRT calib step\n")
           
    ##################################################################################################
    nextstep("TAR'ing files")
    ################################################################################################## 
    try:
        # Getting list of files to be compressed
        files_list=glob.glob(outputFile+".*")
        # Compressing
        tar = tarfile.open(outputFile, "w:gz")
        print("Compressing files in %s output file:" % outputFile)
        for file in files_list:
            print("\t-",file)
            tar.add(file)
        tar.close()
    except OSError as e:
        print("ERROR: Failed compressing the output files\n",e)
        sys.exit(e.errno)    

    # Prints all types of txt files present in a Path
    print("\nListing files:")
    for file in sorted(glob.glob("./*", recursive=True)):
        print("\t-",file)            
